import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {DxButtonModule, DxTextBoxModule, DxValidatorModule} from 'devextreme-angular';

import { AppComponent } from './app.component';
import {NavbarComponent} from "./components/navbar/navbar.component";
import {HomeComponent} from "./components/home/home.component";
import {RouterModule} from "@angular/router";
import {LoginComponent} from "./components/login/login.component";
import {HomeRoutingModule} from "./components/home/home-routing.module";

@NgModule({
  declarations: [
    AppComponent, NavbarComponent, HomeComponent, LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HomeRoutingModule,
    DxButtonModule,
    DxTextBoxModule,
    DxValidatorModule,
    RouterModule.forRoot([
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: '',
        component: HomeComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
