/**
 * Created by deko on 16/12/2016.
 */
import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  /*
     serve per applicare eventuali CSS del component
     anche a livelli esterni. In questo caso ad esempio
     l'immagine di background va applicata al body della pagina,
     ovvero al di fuori dello 'scope' del component stesso.
   */
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./style/login.component.css']
})

/*export class LoginUser {
  public username : string;
  public password : string;
}
*/

export class LoginComponent  {
  constructor() {
  }
}

