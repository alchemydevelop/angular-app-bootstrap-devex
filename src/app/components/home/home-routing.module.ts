import {Routes, RouterModule} from "@angular/router";
import {HomeComponent} from "./home.component";
import {LoginComponent} from "../login/login.component";
import {NgModule} from "@angular/core";
/**
 * Created by deko on 22/12/2016.
 */
const homeRouting: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(homeRouting)
  ],
  exports: [
    RouterModule
  ]
})

export class HomeRoutingModule {}
